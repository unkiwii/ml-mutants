const { spawn } = require('child_process')
const request = require('request')
const test = require('tape')
const util = require('util')
const _ = require('lodash')

const port = 5050
const env = Object.assign({}, process.env, {PORT: port})
const api = 'http://127.0.0.1:' + port

function mutantRequest(body, callback) {
  const child = spawn('node', ['index.js'], {env})
  child.stdout.once('data', _ => {
    request({
      method: 'POST',
      headers: {
        'Content-Type': 'application/json'
      },
      uri: api + '/mutant',
      json: body
    }, (err, res) => {
      child.kill()
      callback(err, res)
    })
  })
}

function testStatus(t, data, expected, error, response) {
  t.false(error, 'should not get and error for ' + data + 'instead got ' + error)
  t.ok(response, 'should have a response')
  const actual = _.get(response, 'statusCode', 0)
  t.equal(actual, expected, 'status should be ' + expected + ' for ' + data)
}

function sequential(list, test) {
  const l = [].concat(list)
  ;(function rec() {
    if (l.length) {
      test(l.pop(), rec)
    }
  })()
}

test('responds 403 on invalid data', t => {
  const invalids = [
    null, undefined, '', {}, [], 123, 'ACTG', 'actg', 'wxyz', 'WXYZ'
  ]
  t.plan(3 * invalids.length)
  sequential(invalids, (invalid, callback) => {
    mutantRequest(invalid, (error, response) => {
      testStatus(t, util.inspect(invalid), 403, error, response)
      callback()
    })
  })
})

test('responds 403 on non-mutant dna', t => {
  const nonMutants = [
    {dna: ['AATA', 'ATGA', 'AGAT', 'TAGA']},
    {dna: ['ATG', 'TGA', 'AGT', 'TTA']},
    {dna: ['CGGG', 'GCGG', 'GGGC', 'GGCG']},
    {dna: ['CGCC', 'GCCC', 'CCGC', 'CCCG']},
    {dna: ['ACT', 'CTG', 'TGA', 'CAG']},
    {dna: ['ACTGA', 'ACTGC', 'ACTGT', 'TTATA']}
  ]
  t.plan(3 * nonMutants.length)
  sequential(nonMutants, (nonMutant, callback) => {
    mutantRequest(nonMutant, (error, response) => {
      testStatus(t, util.inspect(nonMutant), 403, error, response)
      callback()
    })
  })
})

test('responds 200 on mutant dna', t => {
  const mutants = [
    {dna: ['AAAA', 'AAAA', 'AAAA', 'AAAA']},
    {dna: ['TTTT', 'TTTT', 'TTTT', 'TTTT']},
    {dna: ['GGGG', 'GGGG', 'GGGG', 'GGGG']},
    {dna: ['CCCC', 'CCCC', 'CCCC', 'CCCC']},
    {dna: ['ACTG', 'ACTG', 'ACTG', 'ACTG']},
    {dna: ['ACTGA', 'ACTGC', 'ACTGT', 'ACTGG']},
    {dna: ['ATGCGA', 'CAGTGC', 'TTATGT', 'AGAAGG', 'CCCCTA', 'TCACTG']}
  ]
  t.plan(3 * mutants.length)
  sequential(mutants, (mutant, callback) => {
    mutantRequest(mutant, (error, response) => {
      testStatus(t, util.inspect(mutant), 200, error, response)
      callback()
    })
  })
})
