const _ = require('lodash')

const store = require('./store')
const isMutant = require('./is-mutant')(store)
const stats = require('./stats')(store)

const port = process.env.PORT || 5000

const restify = require('restify')

const server = restify.createServer()
server.use(restify.plugins.jsonBodyParser())

server.get(/\/stat/, (req, res) => {
  stats.all().then(all => {
    res.send(all)
  })
})

server.post(/\/mutant/, (req, res) => {
  try {
    if (typeof req.body === 'string') {
      // to support request with no Content-Type header
      req.body = JSON.parse(req.body)
    }
    const dna = _.get(req, 'body.dna', null)
    const statusCode = isMutant(dna) ? 200 : 403
    console.log(new Date().toUTCString(), 'POST /mutant', 'body <', _.get(req, 'body', null), '>', statusCode)
    res.send(statusCode)
  } catch (err) {
    console.log(new Date().toUTCString(), 'POST /mutant', 'ERROR', err.message)
    res.send(403, err)
  }
})

server.listen(port, _ => {
  console.log('running on port', port)
})
