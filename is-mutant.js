const _ = require('lodash')

const CHAIN_SIZE = 4

function chainSizeInGrid(x, y, xdir, ydir, element, grid) {
  let chain = 0
  while (y < grid.length && x < grid[y].length) {
    const el = grid[y][x]
    if (el === element) {
      chain++
    } else {
      return chain
    }
    x += xdir
    y += ydir
  }
  return chain
}

module.exports = function (store) {
  return dna => {
    if (!dna || !_.isArray(dna) || !dna.length) {
      return false
    }

    let chains = 0

    const grid = dna.map(word => word.split(''))
    for (let y = 0, ylen = grid.length; y < ylen; y++) {
      const row = grid[y];
      for (let x = 0, xlen = row.length; x < xlen; x++) {
        const element = row[x]
        if (chainSizeInGrid(x, y, +1, 0, element, grid) >= CHAIN_SIZE) {
          chains++
        }
        if (chainSizeInGrid(x, y, 0, +1, element, grid) >= CHAIN_SIZE) {
          chains++
        }
        if (chainSizeInGrid(x, y, +1, +1, element, grid) >= CHAIN_SIZE) {
          chains++
        }
      }
    }

    const mutant = chains > 1

    store.save(dna, mutant)

    return mutant
  }
}
