# Exercise for a work offer at ML

## Running Locally

Make sure you have [Node.js](http://nodejs.org/) and [Redis](https://redis.io)

Install and open a redis instance on localhost and default port (6379) before you run the server

```sh
$ git clone git clone git@gitlab.com:unkiwii/ml-mutants.git # or clone your own fork
$ cd ml-mutants
$ npm install
$ npm start
```

Your app should now be running on [localhost:5000](http://localhost:5000/).
