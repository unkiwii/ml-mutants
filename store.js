const _ = require('lodash')
const redis = require('redis')
const client = redis.createClient({
  retry_strategy: options => options.error
})

client.on('error', err => console.log('REDIS ERROR:', err))

function key(dna) {
  return dna.reduce((acc, curr) => {
    let ans = acc
    if (!_.isEmpty(curr)) {
      if (!_.isEmpty(acc)) {
        ans += '_'
      }
      ans += curr
    }
    return ans
  }, '')
}

const HASH_KEY = 'dnas'

function connection() {
  if (client.connected) {
    return client
  }

  /**
   * if we have no connection to redis, then return a client that does nothing
   */
  return {
    hset: () => {},
    hgetall: (_, cb) => { cb(null, {}) }
  }
}

module.exports = {
  save: (dna, isMutant) => connection().hset(HASH_KEY, key(dna), isMutant),

  nonMutants: _ => new Promise((resolve, reject) => {
    connection().hgetall(HASH_KEY, (err, replies) => {
      if (err) {
        return reject(err)
      }
      resolve(Object.keys(replies || []).filter(key => replies[key] === 'false'))
    })
  }),

  mutants: _ => new Promise((resolve, reject) => {
    connection().hgetall(HASH_KEY, (err, replies) => {
      if (err) {
        return reject(err)
      }
      resolve(Object.keys(replies || []).filter(key => replies[key] === 'true'))
    })
  })
}
