module.exports = function (store) {
  return {
    all: () =>
      Promise.all([
        store.mutants(),
        store.nonMutants()
      ]).then(([mutants, nonMutants]) => {
        const mutants_count = mutants.length || 0
        const non_mutants_count = nonMutants.length || 0
        const humans_count = mutants_count + non_mutants_count

        let ratio = 0
        if (humans_count > 0) {
          ratio = mutants_count / humans_count
        }

        return {
          count_mutant_dna: mutants_count,
          count_human_dna: humans_count,
          ratio: ratio
        }
      }).catch(_ => ({
        count_mutant_dna: 0,
        count_human_dna: 0,
        ratio: 0
      }))
  }
}
